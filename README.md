# Nintendo's Not-So-Random Sequence

Auxiliary files related to the paper "Entropy Lost: Nintendo’s Not-So-Random Sequence of 32,767 Bits" published at the 13th Workshop on Procedural Content Generation (PCG 2022) can be found in the `PCG22` folder.

The not-so-random sequence of bits is in `32767bits.txt`.
The bgrep regex used to find these matches is in `fingerprint.re`.
The various lists of matches are in the `matches` subfolder.
The conference paper is in `LFSR15_PCG22.pdf`.

To recreate the findings of the paper do the following:
1. Install bgrep by nneoneo [(link)](https://github.com/nneonneo/bgrep]) via `wget 'https://raw.githubusercontent.com/nneonneo/bgrep/master/bgrep.py' -O /usr/local/bin/bgrep`
2. Use the Internet Archive to download recent copies of the *No-Intro* NES/Famicom [(sample link)](https://archive.org/details/nointro.nes) and FDS ROMs [(sample link)](https://archive.org/details/nintendofamilycomputerdisksystemfds20190815131811).
This step assumes that you will use the ROMs for educational purposes only.
Note: Different matches will be obtained using different versions of these ROM sets.
3. If the ROMs are compressed (e.g., `.zip` or `.7z`) then uncompress them.
For example, in Linux you may use `unzip "*.zip"` or `7za -y x "*.7z"`
4. To search for a match in a ROM called `game.nes` use `bgrep -E -f fingerprint.re game.nes`
5. To search every ROM in a folder called ROMs use `bgrep -E -f fingerprint.re ROMs/*`

For example, searching the Japanese version of *Donkey Kong* with `bgrep -E -f fingerprint.re "Donkey Kong (Japan).nes"` results in the following:

```
Donkey Kong (Japan).nes:00003504: 009d31038e300360a51829028500a5192902450018f0013866186619661a661b661c  ..1..0.`..).....).E....8f.f.f.f.f.
```

where `a51829028500a5192902450018f001386618` is the machine code matching the fingerprint of the basic 15-bit LFSR.

The `Journal` folder contains files related to a planned extended version of the paper.
